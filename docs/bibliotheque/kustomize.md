# KUSTOMIZE

[Kustomize](https://kustomize.io/) est une solution permettant de gérer des déclinaisons de descripteurs, elle est inclus dans votre livre de sort sous 2 formes :

- avec l'utilisation de l'option `-k` dans kubectl
- via le binaire `kustomize` qui est à préférer car plus riche que `kubectl -k`

Kustomize est basé sur un principe de modification des descripteurs dit de "base" par de la surcharge. Par exemple cette structure kustomize

```

├── base
│   ├── deployment.yml
│   ├── ingress.yml
│   ├── kustomization.yml
│   └── svc.yml
└── overlays
    ├── production
    │   ├── deployment.yml
    │   ├── ingress.yml
    │   └── kustomization.yml
    └── staging
        ├── deployment.yml
        ├── ingress.yml
        └── kustomization.yml

```

Contient des manifest des object kubernetes `deployment`, `ingress` et `service`, avec une première définition de "base" (dans le dossier `base`) et quelques surcharges pour les environnements "production" et "staging" (dans le dossier `overlays`). Ces surchages sont les petites différences qui "patcherons" la base.

La liste des manifest et des surcharges à appliquer sont définit dans les fichiers `kustomization.yml`. Outre les surchage il existe aussi un système dit de `generator` pour des choses un peu spécifique (comme des confimap ou des secrets par exemple, mais ce ne sera pas utile pour notre aventure)

## Autocompletion commande helm sur bash

```
# load completion for Bash
source <(kustomize completion bash)

# install for Bash in Linux
kustomize completion bash > /etc/bash_completion.d/kustomize
```

(kustomize offre de la complétion sur bash, zsh, fish et powershell)

## Generer les manifest "standard" d'un overlay kustomize (sur la sortie console)

```
kustomize build ./overlays/production/
```

Si l'on chaine cela avec ` | kubectl apply -f-` cela installe le resultat produit par kustomize
